package spectre.connector;

import org.springframework.beans.factory.annotation.Value;

import com.amazonaws.auth.AWSCredentialsProvider;
import com.amazonaws.auth.DefaultAWSCredentialsProviderChain;
import com.amazonaws.client.builder.AwsClientBuilder;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDB;
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder;
import com.amazonaws.services.dynamodbv2.document.DeleteItemOutcome;
import com.amazonaws.services.dynamodbv2.document.DynamoDB;
import com.amazonaws.services.dynamodbv2.document.Item;
import com.amazonaws.services.dynamodbv2.document.Table;
import com.amazonaws.services.dynamodbv2.document.spec.DeleteItemSpec;
import com.amazonaws.services.dynamodbv2.document.spec.GetItemSpec;

public class DynamoDbConnector {
	
	@Value("${dynamoDB.table}")
	private String whitelistTablename;
	
	@Value("${dynamoDB.url}")
	private String whitelistUrl;
	
	@Value("${dynamoDB.region}")
	private String whitelistRegion;
	
	public boolean isWhitelisted(String msisdn) {
		AWSCredentialsProvider awsCredentials = new DefaultAWSCredentialsProviderChain();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withCredentials(awsCredentials)
	            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(whitelistUrl, whitelistRegion))
	            .build();
		DynamoDB dynamoDB = new DynamoDB(client);
		Table whitelistTable = dynamoDB.getTable(whitelistTablename);
		
        GetItemSpec spec = new GetItemSpec().withPrimaryKey("msisdn", msisdn);
        Item outcome = whitelistTable.getItem(spec);
		return outcome != null;
	}
	
	public void deleteFromWhitelist(String msisdn) {
		AWSCredentialsProvider awsCredentials = new DefaultAWSCredentialsProviderChain();
		AmazonDynamoDB client = AmazonDynamoDBClientBuilder.standard().withCredentials(awsCredentials)
	            .withEndpointConfiguration(new AwsClientBuilder.EndpointConfiguration(whitelistUrl, whitelistRegion))
	            .build();
		DynamoDB dynamoDB = new DynamoDB(client);
		Table whitelistTable = dynamoDB.getTable(whitelistTablename);
		
		DeleteItemSpec deleteItemSpec = new DeleteItemSpec().withPrimaryKey("msisdn", msisdn);
		DeleteItemOutcome outcome = whitelistTable.deleteItem(deleteItemSpec);
		System.out.println("Delete from whitelist: " + outcome);
	}
}
