/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package spectre.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.security.InvalidAlgorithmParameterException;
import java.security.InvalidKeyException;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.crypto.*;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;
import org.apache.commons.codec.binary.Base64;
import org.apache.commons.codec.binary.Hex;
import org.springframework.stereotype.Component;

/**
 *
 * @author atvallente
 */
@Component
public class Hashing {

	public synchronized String encrypt(String text, String password, String ivkey)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

		Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
		SecretKeySpec key = new SecretKeySpec(this.md5(this.md5(password)).getBytes(), "AES");
		cipher.init(Cipher.ENCRYPT_MODE, key, new IvParameterSpec(ivkey.getBytes()));

		return Base64.encodeBase64String(cipher.doFinal(text.getBytes()));
	}

	public synchronized String decrypt(String text, String password, String ivkey)
			throws NoSuchAlgorithmException, NoSuchPaddingException, InvalidKeyException,
			InvalidAlgorithmParameterException, IllegalBlockSizeException, BadPaddingException {

		// base64 decoding
		int i = 0;
		String enc64 = text;
		byte[] enc64bytes = enc64.getBytes();
		byte[] dec64bytes = Base64.decodeBase64(enc64bytes);

		ByteArrayInputStream fis = null;
		CipherInputStream cis = null;
		ByteArrayOutputStream fos = null;

		Cipher cipher = Cipher.getInstance("AES/CBC/NoPadding");
		SecretKeySpec key = new SecretKeySpec(this.md5(this.md5(password)).getBytes(), "AES");
		cipher.init(Cipher.DECRYPT_MODE, key, new IvParameterSpec(ivkey.getBytes()));

		fis = new ByteArrayInputStream(dec64bytes);
		cis = new CipherInputStream(fis, cipher);
		fos = new ByteArrayOutputStream();

		try {
			// decrypting
			byte[] b = new byte[8];
			while ((i = cis.read(b)) != -1) {
				fos.write(b, 0, i);
			}
			fos.flush();
			fos.close();
			cis.close();
			fis.close();
		} catch (IOException ex) {
			Logger.getLogger(Hashing.class.getName()).log(Level.SEVERE, null, ex);
		}

		return fos.toString();
	}

	public synchronized String md5(String message) {
		String digest = null;
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			byte[] hash = md.digest(message.getBytes("UTF-8"));

			// converting byte array to Hexadecimal String
			StringBuilder sb = new StringBuilder(2 * hash.length);
			for (byte b : hash) {
				sb.append(String.format("%02x", b & 0xff));
			}
			digest = sb.toString();
		} catch (UnsupportedEncodingException uex) {
			System.out.println("ERROR: UnsupportedEncodingException md5() >> " + uex.getMessage().toString());
		} catch (NoSuchAlgorithmException ex) {
			System.out.println("ERROR: NoSuchAlgorithmException md5() >> " + ex.getMessage().toString());
		}
		return digest;
	}

	public synchronized String sha1(String message) {
		MessageDigest digest = null;
		StringBuffer sb = new StringBuffer();
		try {
			digest = MessageDigest.getInstance("SHA1");
			byte[] result = digest.digest(message.getBytes());

			for (int i = 0; i < result.length; i++) {
				sb.append(Integer.toString((result[i] & 0xff) + 0x100, 16).substring(1));
			}

		} catch (NoSuchAlgorithmException ex) {
			System.out.println("ERROR: NoSuchAlgorithmException sha1() >> " + ex.getMessage().toString());
		}
		return sb.toString();
	}

	public synchronized String sha256(String message) {
		MessageDigest digest = null;
		StringBuffer sb = new StringBuffer();
		try {
			digest = MessageDigest.getInstance("SHA-256");
			digest.update(message.getBytes());

			byte byteData[] = digest.digest();

			for (int i = 0; i < byteData.length; i++) {
				sb.append(Integer.toString((byteData[i] & 0xff) + 0x100, 16).substring(1));
			}

		} catch (NoSuchAlgorithmException ex) {
			System.out.println("ERROR: NoSuchAlgorithmException sha256() >> " + ex.getMessage().toString());
		}
		return sb.toString();
	}

	public synchronized String hmacSha256(String secret, String message) {
		String hash = "";
		try {
			Mac sha256_HMAC = Mac.getInstance("HmacSHA256");
			SecretKeySpec secret_key = new SecretKeySpec(secret.getBytes(), "HmacSHA256");
			sha256_HMAC.init(secret_key);
			hash = new String(Hex.encodeHex(sha256_HMAC.doFinal(message.getBytes("UTF-8"))));
		} catch (Exception e) {
			System.out.println("Hasing Error " + e.getMessage());
		}
		return hash.trim();
	}

	public synchronized String doGenerateHMAC(String key, String timestamp, int carrier) throws Exception {

		Mac sha256_HMAC = Mac.getInstance("HmacSHA256");

		String str = timestamp + carrier;

		SecretKeySpec secret_key = new SecretKeySpec(key.getBytes("UTF-8"), "HmacSHA256");
		sha256_HMAC.init(secret_key);
		return new String(Hex.encodeHex(sha256_HMAC.doFinal(str.getBytes("UTF-8"))));
	}
}
