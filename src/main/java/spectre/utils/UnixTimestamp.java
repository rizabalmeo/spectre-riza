package spectre.utils;

import java.util.Date;

public class UnixTimestamp {

	public int getTimestamp() {
        return Math.round(new Date().getTime() / 1000);
    }
}
